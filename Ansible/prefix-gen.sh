#!/bin/bash

# Script by Anurag Bhatia (anurag@he.net)

# Provide ASN or AS-SET while calling the script. 
# Example usage ./prefix-gen.sh airtel as9498

bgpq3 -s $2 | sed '/^$/d' | grep -v "no ip prefix-list" | awk -F ' ' '{OFS="|"; print $5,$7}' > prefix-list.txt

# # Delete existing prefix list to keep latest data only 
# echo "delete policy prefix-list $1"

cat prefix-list.txt | while read data 

do 
prefix_seq=`echo $data | awk -F '|' '{print $1}'`
prefix=`echo $data | awk -F '|' '{print $2}'`

echo -e "set policy prefix-list $1 rule $prefix_seq action 'permit'
set policy prefix-list $1 rule $prefix_seq prefix '$prefix'"
 
done

rm prefix-list.txt
